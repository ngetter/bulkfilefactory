﻿"""
Spyder Editor

This is a general file iterator
the purpose of this code is to iterate all files with specific extention
in a directory and apply a function on each file
"""
import os
import time
import re
#decorator function:
#loop through the filtered file list and execute a files_action() on each file
def loopFiles(action_):
    
    def newFunc(*args,**kwargs):
        for it_fn in kwargs['file_list']:
            print('\n{}\n{}'.format(it_fn, time.strftime('%H:%M')))
            t = time.time()
            
            #define this function for iterating files    
            action_(it_fn, *args, **kwargs)
            
            t = time.time() - t
            gt = time.gmtime(t)
            print('time passed {} seconds'.format(
                gt.tm_min * 60 + gt.tm_sec)
                )
            print ('_' * 20)
    return newFunc


def fileFilter(wdir, ext, regex = None):
    ldir = [ os.path.join(wdir, fpath) for fpath in os.listdir(wdir)]
    
    # get directories within wdir
    ldirs = [f for f in ldir if os.path.isdir(f)]
    
    tmp_list = []
    for  directory in ldirs:
            #filter the file list in two phases:
            #   1. exlude all non files entries
            #   2. include only files with the preffered extention
            lfiles = [ os.path.join(directory,fpath) for fpath in os.listdir(directory) ]
            filtered_file_list = [f for f in lfiles if os.path.isfile(f)]
            
            # if regex is defined, use this exclusively for the filter
            # use extenstion filtering only whrere there is no regex
            if regex:

                regxc = re.compile(regex)
                filtered_file_list = [f for f in filtered_file_list 
                                    if regxc.search(f) ] 
            else: # no regex defined
                filtered_file_list = [f for f in filtered_file_list 
                                    if os.path.splitext(f)[1]==ext ]                
            
            # flatten the filtered list into the tmp_list                        
            [tmp_list.append(f) for f in filtered_file_list] 
    return tmp_list
    

def getExportDir(wdir, exportTo):
    exportd = os.path.join(wdir, exportTo)
    if not os.path.exists( exportd ):
        os.makedirs( exportd )
    return exportd