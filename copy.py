# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 16:04:40 2016

@author: cognitive
"""

import os
import sys   
import shutil as sh

from BulkFileFactory import loopFiles, fileFilter, getExportDir
#set the working dir from first argument or set current working directory 
if len(sys.argv) > 1:
    wdir = sys.argv[1]
else:
    wdir = os.getcwd()


@loopFiles
def copyFiles(filename, expd, *args, **kwargs):
     print('{} --> {}'.format(filename, expd))
     sh.copy2(filename, expd)
    
EXT = '.set'
EXPORT_DIR = r'G:/research_data/Shirly/data/artifactsfree/PRE_EO'
#list the files in working directory with their full path

filtered_file_list= fileFilter(wdir, EXT, r'PRE_EC.fdt$')

#set the directory for the exported files
#create one if not exists
exportd = getExportDir(wdir, EXPORT_DIR)
    

#renameBySemantics( exportd=exportd, file_list=filtered_file_list)
copyFiles(expd = exportd, file_list=filtered_file_list)
