# -*- coding: utf-8 -*-
"""
Created on Sun Jun 26 13:34:36 2016

@author: Nir
"""
import sys
import pandas as pn

from pandas import DataFrame as df
from os.path import basename

from BulkFileFactory import loopFiles, fileFilter
electrodes = {
    'FP1':1,
    'FP2':10,
    'F7':6,
    'F3':2,
    'Fz':9,
    'F4':11,
    'F8':15,
    'T3':7,
    'C3':3,
    'Cz':18,
    'C4':12,
    'T4':16,
    'T5':8,
    'P3':4,
    'Pz':19,
    'P4':13,
    'T6':17,
    'O1':5,
    'O2':14
}
electrodes = {value:key for (key,value) in electrodes.items()}

@loopFiles
def concat(dat, df_merged,  *args, **kwargs):
    try:
        tmp = df.from_csv(dat, header=None)#kwargs['import_from_line'] )
        print(dat)
    except(IOError):
        print ('ERROR: {} cannot be imported - probably a folder').format(dat)
        
    # analyze file name and extrut data coded within
    basefile = basename(dat)
    parpams = basefile.split('_')
    subj = parpams[0].split("S")[1]
    
    # add extra data to the filtered data
    tmp['channel'] = electrodes.values()
    tmp['subject'] = subj
    tmp['session'] = parpams[1]
    tmp['eyes'] = parpams[2]

    # append to a meta list
    df_merged.append(tmp)


# configuration flags
DATA_FOLDER = sys.argv[1]
IMPORT_FROM_LINE = 1

df_summarize = []
df_merged = []

files = fileFilter(DATA_FOLDER, '.csv', r'(?<!ica)_spectra.csv$')
concat(file_list=files, df_merged=df_merged, import_from_line = IMPORT_FROM_LINE)

merged = pn.concat(df_merged).reset_index()
merged.to_csv('{}\\subjects-spectra-concatenated.csv'.format(DATA_FOLDER))
