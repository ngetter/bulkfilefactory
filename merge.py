# -*- coding: utf-8 -*-
"""
Created on Mon Jan 26 10:13:14 2015

@author: students
"""
import os
import pandas as pn
import matplotlib.pyplot as plt

from pandas import DataFrame as df
from numpy import mean

pn.options.display.mpl_style = 'default'

# configuration flags
CHARTS_OUTPUT = True
DATA_FOLDER = 'exact'
IMPORT_FROM_LINE = 7

# factor to subject assignment
treatments = df.from_csv('participants.csv')
treatments = treatments['E'].apply(
    lambda x: {'1':'IPS', '4':'IPS','2':'Temp','3':'Temp'}.get(str(x)))
    

df_summarize = []
df_merged = []
for dat in os.listdir('./{}'.format(DATA_FOLDER) ):
    try:
        tmp = df.from_csv('./{}/{}'.format(DATA_FOLDER,dat), header=IMPORT_FROM_LINE )
        print(dat)
    except(IOError):
        print ('./{} cannot be imported - probably a folder').format(dat)
        continue
    # analyze file name and extrut data coded within
    _, subj, session = dat.split('_')
    session = session.split('.')[0]
    subj = subj[-2:]
    
    # calculate the overall accuracy for the file       
    tmp_acc = tmp.correct.mean()
    
    # filter out all the observations with error 
    filtered = tmp[tmp['correct'] == 1]
    
    # calculate the reaction time for that file
    tmp_rt = tmp.response_time.mean()
    
    # add extra data to the filtered data
    filtered['subject'] = subj
    filtered['session'] = session
    filtered['file'] = dat
    filtered['ACC'] = tmp_acc
    filtered['treatment'] = treatments.loc[[int(subj)]].iloc[0]

    # append to a meta list
    df_merged.append(filtered)

merged = pn.concat(df_merged).reset_index()
merged.to_csv('{}_merged.csv'.format(DATA_FOLDER))

subset = merged[['file','subject','session','treatment', 'calculation','response_time']]
##subset['treatment'] = subset['subject'].apply(lambda x: treatments.loc[[int(x)]].iloc[0])
subset.to_csv('{}_minimal.csv'.format(DATA_FOLDER))

gb = subset.groupby(['subject','session','treatment'])
agg = gb.agg(mean)
agg.to_csv('{}_agg_anac.csv'.format(DATA_FOLDER))

#==============================================================================
# stacked = agg.unstack(level=['session','treatment'])
# stacked.response_time.to_csv('{}_rt_stacked.csv'.format(DATA_FOLDER))
#==============================================================================



