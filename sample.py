# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 16:04:40 2016

@author: cognitive
"""

import os
import sys   

from BulkFileFactory import loopFiles, fileFilter, getExportDir
#set the working dir from first argument or set current working directory 
if len(sys.argv) > 1:
    wdir = sys.argv[1]
else:
    wdir = os.getcwd()


# rename by file name semantics
@loopFiles
def renameBySemantics(it_fn, *args, **kwargs):
    params = os.path.split(it_fn)[-1].split('_')    
    sid = params[0][-2:]
    new_fn = '{}_{}'.format(sid, '_'.join(params[1:]))
    os.rename(it_fn, os.path.join(kwargs['exportd'], new_fn))


@loopFiles
def printFiles(filename, expd, *args, **kwargs):
    print('{} --> {}'.format(filename, expd))
    
EXT = '.txt'
EXPORT_DIR = r'test'
#list the files in working directory with their full path

filtered_file_list= fileFilter(wdir, EXT)

#set the directory for the exported files
#create one if not exists
exportd = getExportDir(wdir, EXPORT_DIR)
    

#renameBySemantics( exportd=exportd, file_list=filtered_file_list)
printFiles(expd = exportd, file_list=filtered_file_list)
